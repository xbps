# Template file for 'wicd'
pkgname=wicd
version=1.7.0
revision=1
distfiles="${SOURCEFORGE_SITE}/$pkgname/$pkgname-$version.tar.bz2"
build_style=custom-install
short_desc="Open source wired and wireless network manager"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=14c5692bf8a4c93c9ac083ce9ad677b0d9022b19f84e777854766b2a6f753b32
long_desc="
 Wicd is an open source wired and wireless network manager for Linux
 which aims to provide a simple interface to connect to networks with a
 wide variety of settings.

 Some of Wicd's features include:
   * No Gnome dependencies (although it does require GTK), so it is easy to
     use in XFCE, Fluxbox, Openbox, Enlightenment, etc.
   * Ability to connect to wired (Ethernet only, no PPPoE/DSL support yet)
     and wireless networks
   * Profiles for each wireless network and wired network
   * Many encryption schemes, some of which include WEP/WPA/WPA2 (and you
     can add your own)
   * Remains compatible with wireless-tools
   * Tray icon showing network activity and signal strength
   * A full-featured console interface"

keep_empty_dirs=yes
noarch=yes
conf_files="
/etc/dbus-1/system.d/wicd.conf
/etc/wicd/encryption/templates/active"
gtk_iconcache_dirs="/usr/share/icons/hicolor"
openrc_services="wicd default false"

Add_dependency full dbus-python
Add_dependency full pygtk
Add_dependency full dhcpcd
Add_dependency full wpa_supplicant
Add_dependency full wireless_tools
Add_dependency full ethtool
Add_dependency full shared-mime-info
Add_dependency full python-urwid	">=0.9.9"
Add_dependency full hicolor-icon-theme
Add_dependency full desktop-file-utils
Add_dependency full notify-python

do_install()
{
	cd ${wrksrc} || return 1
	python setup.py configure --no-install-init \
		--resume=/usr/share/wicd/scripts \
		--suspend=/usr/share/wicd/scripts || return 1
	python setup.py install --root=${DESTDIR} || return 1

	install -D -m755 ${FILESDIR}/wicd.rc \
		${DESTDIR}/etc/init.d/wicd || return 1
	install -d ${DESTDIR}/usr/share/applications || return 1
	install -m644 ${FILESDIR}/wicd.desktop \
		${DESTDIR}/usr/share/applications || return 1
	install -d ${DESTDIR}/usr/lib/wicd || return 1
	install -D -m755 build/lib/wicd/*.py \
		${DESTDIR}/usr/lib/wicd || return 1
}
