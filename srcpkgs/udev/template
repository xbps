# Template file for 'udev'
pkgname=udev
version=153
distfiles="${KERNEL_SITE}/utils/kernel/hotplug/udev-${version}.tar.bz2"
build_style=gnu_configure
configure_args="--exec-prefix= --without-selinux --libexecdir=/lib/udev
 --with-rootlibdir=/lib --enable-introspection"
short_desc="A userspace implementation of devfs"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=8a0e6b8d12284c03652bcbfa26156308f1ee3288f9a3499088cc2e0731d17644
long_desc="
 udev is a implementation of devfs in userspace using sysfs and
 /sbin/hotplug. It requires a 2.5/2.6 kernel to run properly."

keep_empty_dirs=yes
openrc_services="udev sysinit true"
conf_files="/etc/udev/udev.conf"
subpackages="gir-gudev libudev libudev-devel libgudev libgudev-devel"
triggers="initramfs-tools"

Add_dependency run glibc
Add_dependency run acl
Add_dependency run glib
Add_dependency run libusb-compat
Add_dependency run libudev
Add_dependency full usbutils	">=0.82"
Add_dependency full pciutils
Add_dependency build pkg-config
Add_dependency build libtool
Add_dependency build gperf
Add_dependency build glib-devel
Add_dependency build acl-devel
Add_dependency build glib-devel
Add_dependency build libusb-compat-devel
Add_dependency build gobject-introspection

post_install()
{
	# Install some additional rules.
	install -m644 ${wrksrc}/rules/packages/* ${DESTDIR}/lib/udev/rules.d
	# Remove unneeded rules.
	for f in ia64 ppc s390; do
		rm ${DESTDIR}/lib/udev/rules.d/40-${f}.rules
	done

	# Install the OpenRC service
	install -D -m755 ${FILESDIR}/udev.rc ${DESTDIR}/etc/init.d/udev

        # Install the initramfs-tools hook/scripts.
	install -d $DESTDIR/usr/share/initramfs-tools/hooks
	install -d $DESTDIR/usr/share/initramfs-tools/scripts/init-top
	install -d $DESTDIR/usr/share/initramfs-tools/scripts/init-bottom
	install -m 755 ${FILESDIR}/udev.initramfs-hook \
		$DESTDIR/usr/share/initramfs-tools/hooks/udev
	install -m 755 ${FILESDIR}/udev.initramfs-inittop \
		$DESTDIR/usr/share/initramfs-tools/scripts/init-top/udev
	install -m 755 ${FILESDIR}/udev.initramfs-bottom \
		$DESTDIR/usr/share/initramfs-tools/scripts/init-bottom/udev

	# Move gobject introspection stuff to correct path
	mv $DESTDIR//lib/girepository* $DESTDIR/usr/lib

	# Move pkgconfig files to correct path
	install -d $DESTDIR/usr/lib/pkgconfig
	mv $DESTDIR/lib/pkgconfig/* $DESTDIR/usr/lib/pkgconfig
	mv $DESTDIR/usr/share/pkgconfig/* $DESTDIR/usr/lib/pkgconfig
	rmdir $DESTDIR/usr/share/pkgconfig
	rmdir $DESTDIR/lib/pkgconfig
}
