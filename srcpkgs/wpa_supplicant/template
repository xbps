# Template file for 'wpa_supplicant'
pkgname=wpa_supplicant
version=0.7.0
revision=2
wrksrc=${pkgname}-${version}/${pkgname}
distfiles="http://hostap.epitest.fi/releases/$pkgname-$version.tar.gz"
build_style=gnu_makefile
make_install_target="PREFIX=/usr
 DESTDIR=${XBPS_DESTDIR}/${pkgname}-${version} install"
short_desc="WPA/WPA2/IEEE 802.1X Supplicant"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=9d34f678881b21eb685b730bfed2dc56f4c9eb50bf79245c246f0b8266b75e21
long_desc="
 $pkgname is a WPA Supplicant for Linux, BSD, Mac OS X, and Windows with
 support for WPA and WPA2 (IEEE 802.11i / RSN). It is suitable for both
 desktop/laptop computers and embedded systems. Supplicant is the IEEE
 802.1X/WPA component that is used in the client stations. It implements
 key negotiation with a WPA Authenticator and it controls the roaming and
 IEEE 802.11 authentication/association of the wlan driver.

 $pkgname is designed to be a daemon program that runs in the background
 and acts as the backend component controlling the wireless connection.
 $pkgname supports separate frontend programs and a text-based frontend
 (wpa_cli) and a GUI (wpa_gui) are included with $pkgname."

conf_files="/etc/${pkgname}.conf"
openrc_services="$pkgname default false"

Add_dependency run glibc
Add_dependency run libnl
Add_dependency run openssl
Add_dependency run dbus-libs
Add_dependency run ncurses-libs
Add_dependency run readline
Add_dependency build libnl-devel
Add_dependency build openssl-devel
Add_dependency build dbus-devel
Add_dependency build ncurses-devel
Add_dependency build readline-devel

pre_build()
{
	sed -i 's|/usr/local|$(PREFIX)|g' ${wrksrc}/Makefile
	cp -f ${FILESDIR}/config ${wrksrc}/.config
}

post_install()
{
	local dbus_srv=fi.epitest.hostap.WPASupplicant.service
	local dbus_conf=fi.epitest.hostap.WPASupplicant.conf

	cd ${wrksrc} || return 1
	install -d ${DESTDIR}/etc/init.d
	install -m755 ${FILESDIR}/${pkgname}.rc \
		${DESTDIR}/etc/init.d/${pkgname}
	install -m644 wpa_supplicant.conf ${DESTDIR}/etc
	for d in 5 8; do
		install -d ${DESTDIR}/usr/share/man/man${d}
	done
	install -m644 doc/docbook/${pkgname}.conf.5 \
		${DESTDIR}/usr/share/man/man5
	install -m644 doc/docbook/wpa_*.8 ${DESTDIR}/usr/share/man/man8
	install -d ${DESTDIR}/usr/share/dbus-1/system-services
	sed -i -e "s|/sbin|/usr/sbin|" dbus-wpa_supplicant.service
	install -m644 dbus-wpa_supplicant.service \
		${DESTDIR}/usr/share/dbus-1/system-services/${dbus_srv}
	install -d ${DESTDIR}/etc/dbus-1/system.d
	install -m644 dbus-wpa_supplicant.conf \
		${DESTDIR}/etc/dbus-1/system.d/${dbus_conf}
}
