# Template file for 'apache'
pkgname=apache
version=2.2.15
wrksrc=httpd-${version}
distfiles="http://www.apache.org/dist/httpd/httpd-${version}.tar.bz2"
build_style=gnu_configure
configure_args="--prefix= --enable-pie --enable-modules=all
--enable-mods-shared=all --enable-authn-dbm --enable-authn-anon
--enable-authn-dbd --enable-authn-alias --enable-authz-dbm
--enable-authz-owner --enable-authnz-ldap --enable-auth-digest -enable-isapi
--enable-file-cache --enable-cache --enable-disk-cache --enable-mem-cache
--enable-dbd --enable-bucketeer --enable-dumpio --enable-echo --enable-reqtimeout
--enable-ext-filter --enable-substitute --enable-deflate --enable-charset-lite
--enable-ldap --enable-log-forensic --enable-logio --enable-mime-magic
--enable-cern-meta --enable-expires --enable-headers --enable-ident
--enable-usertrack --enable-unique-id --enable-proxy --enable-proxy-connect
--enable-proxy-ftp --enable-proxy-http --enable-proxy-scgi --enable-proxy-ajp
--enable-proxy-balancer --enable-ssl --enable-http --enable-speling
--enable-dav --enable-info --enable-cgi --enable-cgid --enable-dav-fs
--enable-dav-lock --enable-vhost-alias --enable-imagemap --enable-so
--enable-rewrite --with-pcre=/usr --enable-layout=XBPS --sysconfdir=/etc/httpd"
short_desc="The Number One HTTP Server On The Internet"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=5ae0c428e7abd87eecbac8564d90a7182104325bae7086c21db7b3a1e3140ca7
long_desc="
 The Apache HTTP Server Project is an effort to develop and maintain an
 open-source HTTP server for modern operating systems including UNIX and
 Windows NT. The goal of this project is to provide a secure, efficient and
 extensible server that provides HTTP services in sync with the current HTTP
 standards."

subpackages="apache-devel"

conf_files="
/etc/conf.d/apache
/etc/httpd/extra/httpd-autoindex.conf
/etc/httpd/extra/httpd-dav.conf
/etc/httpd/extra/httpd-manual.conf
/etc/httpd/extra/httpd-vhosts.conf
/etc/httpd/extra/httpd-info.conf
/etc/httpd/extra/httpd-languages.conf
/etc/httpd/extra/httpd-userdir.conf
/etc/httpd/extra/httpd-ssl.conf
/etc/httpd/extra/httpd-mpm.conf
/etc/httpd/extra/httpd-default.conf
/etc/httpd/extra/httpd-multilang-errordoc.conf
/etc/httpd/httpd.conf
/etc/httpd/magic
/etc/httpd/mime.types"

system_accounts="httpd"
httpd_descr="Apache HTTP server"
httpd_homedir="/srv/httpd"

openrc_services="apache default false"
keep_empty_dirs=yes

Add_dependency run glibc
Add_dependency run zlib
Add_dependency run libuuid
Add_dependency run pcre
Add_dependency run db
Add_dependency run gdbm
Add_dependency run expat
Add_dependency run libldap
Add_dependency run openssl
Add_dependency run apr
Add_dependency run apr-util

Add_dependency build zlib-devel
Add_dependency build libuuid-devel
Add_dependency build pcre-devel
Add_dependency build db-devel
Add_dependency build gdbm-devel
Add_dependency build expat-devel
Add_dependency build libldap-devel
Add_dependency build openssl-devel
Add_dependency build apr-devel
Add_dependency build apr-util-devel

pre_configure()
{
        # set default user
	sed -e 's#User daemon#User httpd#' \
	    -e 's#Group daemon#Group httpd#' \
	    -i ${wrksrc}/docs/conf/httpd.conf.in || return 1

	cat ${FILESDIR}/xbps.layout >> ${wrksrc}/config.layout
}

post_install()
{
	# Enable userdir, languages, autoindex, errors.
	# Set a working ServerRoot, disable mod_unique_id.
	# Set and enable ServerName to 127.0.0.1.
	sed -e 's|#\(Include /etc/httpd/extra/httpd-multilang-errordoc.conf\)|\1|' \
		-e 's|#\(Include /etc/httpd/extra/httpd-autoindex.conf\)|\1|' \
		-e 's|#\(Include /etc/httpd/extra/httpd-languages.conf\)|\1|' \
		-e 's|#\(Include /etc/httpd/extra/httpd-userdir.conf\)|\1|' \
		-e 's|#\(Include /etc/httpd/extra/httpd-default.conf\)|\1|' \
		-e 's|ServerRoot \"\"|ServerRoot \"/etc/httpd\"|' \
		-e 's|#\(ServerName\).*|\1 127.0.0.1:80|' \
		-e 's|\(LoadModule unique_id_module.*\)|#\1|' \
		-i ${DESTDIR}/etc/httpd/httpd.conf || return 1

        # symlinks for /etc/httpd
	ln -fs /var/log/httpd ${DESTDIR}/etc/httpd/logs
	ln -fs /var/run/httpd ${DESTDIR}/etc/httpd/run
	ln -fs /usr/libexec/httpd ${DESTDIR}/etc/httpd/modules
	ln -fs /usr/share/apache-httpd/build ${DESTDIR}/etc/httpd/build

	# Remove unused stuff.
	rm -f ${DESTDIR}/srv/httpd/cgi-bin/test-cgi
	rm -f ${DESTDIR}/srv/httpd/cgi-bin/printenv
	rm -f ${DESTDIR}/srv/httpd/index.html

	install -D -m644 ${FILESDIR}/apache.logrotate \
		${DESTDIR}/etc/logrotate.d/apache || return 1
	install -D -m755 ${FILESDIR}/apache.rc \
		${DESTDIR}/etc/init.d/apache || return 1
	install -D -m644 ${FILESDIR}/apache.confd \
		${DESTDIR}/etc/conf.d/apache || return 1

	rm -rf ${DESTDIR}/etc/httpd/original
}
