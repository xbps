# Template file for 'dnsmasq'
pkgname=dnsmasq
version=2.51
revision=1
distfiles="http://www.thekelleys.org.uk/$pkgname/$pkgname-$version.tar.gz"
build_style=gnu_makefile
make_build_target="all-i18n"
make_install_args="PREFIX=/usr install-i18n"
short_desc="Lightweight, easy to configure DNS forwarder and DHCP server"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=08a7f95e1aa344be07681f514d992320b86932c5dc2f091ac6edd0599ff90622
long_desc="
 Dnsmasq is a lightweight, easy to configure DNS forwarder and DHCP server.
 It is designed to provide DNS and, optionally, DHCP, to a small network. It
 can serve the names of local machines which are not in the global DNS.
 The DHCP server integrates with the DNS server and allows machines with
 DHCP-allocated addresses to appear in the DNS with names configured either in
 each host or in a central configuration file. Dnsmasq supports static and
 dynamic DHCP leases and BOOTP/TFTP/PXE for network booting of diskless machines."

conf_files="/etc/dnsmasq.conf /etc/conf.d/dnsmasq"
openrc_services="dnsmasq default false"

Add_dependency run glibc
Add_dependency run dbus-libs
Add_dependency run libcap
Add_dependency run libidn
Add_dependency build gettext
Add_dependency build dbus-devel
Add_dependency build libcap-devel
Add_dependency build libidn-devel

post_install()
{
	cd ${wrksrc} || return 1
	install -D -m644 dnsmasq.conf.example \
		${DESTDIR}/etc/dnsmasq.conf || return 1
	install -D -m755 ${FILESDIR}/dnsmasq.rc \
		${DESTDIR}/etc/init.d/dnsmasq || return 1
	install -D -m644 ${FILESDIR}/dnsmasq.confd \
		${DESTDIR}/etc/conf.d/dnsmasq || return 1
	install -D -m644 dbus/dnsmasq.conf \
		${DESTDIR}/etc/dbus-1/system.d/dnsmasq.conf || return 1
}
