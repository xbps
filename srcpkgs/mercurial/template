# Template file for 'mercurial'
pkgname=mercurial
version=1.5.1
distfiles="http://mercurial.selenic.com/release/$pkgname-$version.tar.gz"
build_style=python-module
short_desc="Fast, lightweight source control management system"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=5796dd27c884c0effb027c71925fe2c2506b08e0ac8c5f53db259d378ef96569
long_desc="
 Mercurial is a fast, lightweight Source Control Management system
 designed for efficient handling of very large distributed projects.

 Fast
        * Extremely high-performance delta-compressed storage scheme
        * Optimized for disk layout and access efficiency
        * Complete cross-indexing of files and changesets
        * Bandwidth and CPU efficient HTTP and SSH sync protocols

 Scalable
	* Distributed development model supports unlimited numbers
	  of developers
        * Allows arbitrary merging between developer branches
	* Doesn't significantly degrade with large numbers of files
	  or changesets
        * No waiting for locks!

 Robust
        * SHA1 integrity checking on repository data
        * Append-only storage model with transaction journalling
        * Fast full-repository verification
        * Convenient backup

 Easy to use
        * Most commands are familiar to users of CVS and other systems
        * Built-in command help
        * Integrated stand-alone web interface
        * Works with various GUI tools

 Easy to adopt
        * Runs on UNIX, MacOS X, and Windows
        * Conversion tools available for many popular SCMs
        * Allows a variety of usage models
        * Supports user-defined hooks and extensions

 Free
        * Source code available under the GPL license
        * Actively community supported and developed"

Add_dependency run glibc
Add_dependency full python
Add_dependency build gettext

post_install()
{
	cd ${wrksrc} || return 1
	install -d ${DESTDIR}/usr/share/man/man1
	install -d ${DESTDIR}/usr/share/man/man5
	install -m 644 doc/*.1 ${DESTDIR}/usr/share/man/man1
	install -m 644 doc/*.5 ${DESTDIR}/usr/share/man/man5
}
