# Template file for 'xbps-base-files'
pkgname=xbps-base-files
version=0.35
build_style=custom-install
short_desc="xbps base system files"
maintainer="Juan RP <xtraeme@gmail.com>"
long_desc="
 This package installs the base system files and directories that
 aren't installed by any other package and are required on any
 GNU/Linux system."

keep_empty_dirs=yes
base_chroot=yes

conf_files="/etc/profile /etc/hosts /etc/host.conf /etc/securetty
 /etc/skel/.bash_profile /etc/skel/.bash_logout /etc/skel/.bashrc
 /etc/issue"

do_install()
{
	#
	# Install FHS system directories.
	#
	for d in bin sbin boot etc home lib dev proc mnt opt sys var \
		 media; do
		install -d ${DESTDIR}/${d}
	done

	install -d -m750 ${DESTDIR}/root
	for d in tmp var/tmp var/lock var/spool/mail; do
		install -d -m1777 ${DESTDIR}/${d}
	done

	for d in local local/bin local/sbin local/include local/lib \
		 bin include lib sbin src; do
		install -d ${DESTDIR}/usr/${d}
	done

	for d in locale misc terminfo zoneinfo doc info; do
		install -d ${DESTDIR}/usr/share/${d}
		install -d ${DESTDIR}/usr/local/share/${d}
	done

	for f in 1 2 3 4 5 6 7 8; do
		install -d ${DESTDIR}/usr/share/man/man${d}
		install -d ${DESTDIR}/usr/local/share/man/man${d}
	done

	cd ${DESTDIR}/usr && ln -s ./share/man man
	cd ${DESTDIR}/usr/local && ln -s ./share/man man

	for d in log run opt cache lib; do
		install -d ${DESTDIR}/var/${d}
	done

	if [ "$xbps_machine" = "x86_64" ]; then
		cd ${DESTDIR} && ln -s lib lib64
		cd ${DESTDIR}/usr && ln -s lib lib64
	fi

	#
	# Install trigger files.
	#
	install -d ${DESTDIR}/var/db/xbps/triggers
	install -m750 ${XBPS_TRIGGERSDIR}/* ${DESTDIR}/var/db/xbps/triggers

	#
	# Install misc config files.
	#
	for f in bash_logout bash_profile bashrc; do
		install -D -m644 ${FILESDIR}/dot_${f} ${DESTDIR}/etc/skel/.${f}
	done

	install -m644 ${FILESDIR}/profile ${DESTDIR}/etc
	install -m600 ${FILESDIR}/securetty ${DESTDIR}/etc
	install -m644 ${FILESDIR}/hosts ${DESTDIR}/etc
	install -m644 ${FILESDIR}/host.conf ${DESTDIR}/etc
	install -m644 ${FILESDIR}/issue ${DESTDIR}/etc

	install -d ${DESTDIR}/etc/profile.d
	install -m644 ${FILESDIR}/*.sh ${DESTDIR}/etc/profile.d
	install -m644 ${FILESDIR}/DIR_COLORS* ${DESTDIR}/etc

	install -d ${DESTDIR}/etc/modprobe.d
	install -m644 ${FILESDIR}/usb-load-ehci-first \
		${DESTDIR}/etc/modprobe.d/usb-load-ehci-first.conf
	install -m644 ${FILESDIR}/blacklist.conf ${DESTDIR}/etc/modprobe.d
}
