# Template file for 'mit-krb5'
pkgname=mit-krb5
version=1.8
revision=1
wrksrc="krb5-${version}"
distfiles="http://web.mit.edu/Kerberos/dist/krb5/$version/krb5-$version-signed.tar"
build_style=gnu_configure
configure_script="./src/configure"
configure_args="--disable-rpath --enable-kdc-replay-cache --with-system-et
--with-system-ss --with-system-db --enable-shared"
short_desc="MIT Kerberos 5 implementation"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=99602433fad7dab3708974244278a579d7e7f60b08386046f9c1b3ec8824a86b
long_desc="
 Kerberos is a network authentication protocol. It is designed to provide
 strong authentication for client/server applications by using secret-key
 cryptography. A free implementation of this protocol is available from the
 Massachusetts Institute of Technology. Kerberos is available in many
 commercial products as well."

CFLAGS="-I/usr/include/et"

noextract=yes
subpackages="$pkgname-devel $pkgname-libs $pkgname-client"
conflicts="heimdal"

Add_dependency run glibc
Add_dependency run db
Add_dependency run e2fsprogs-libs
Add_dependency run openssl
Add_dependency run mit-krb5-libs
Add_dependency run mit-krb5-client

Add_dependency build flex
Add_dependency build db-devel
Add_dependency build e2fsprogs-devel
Add_dependency build openssl-devel

pre_configure()
{
	# We have to do this dance because the real distfile is
	# included in the .tar archive.
	tar xf ${XBPS_SRCDISTDIR}/$(basename ${distfiles}) \
		-C ${wrksrc} || return 1
	tar xfz ${wrksrc}/krb5-${version}.tar.gz \
		-C ${XBPS_BUILDDIR} || return 1

	# Fix db plugin.
	sed -i -e "s|<db.h>|<db_185.h>|" ${wrksrc}/src/plugins/kdb/db2/*.[ch]
}
