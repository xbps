# Template file for 'heimdal'
pkgname=heimdal
version=1.3.1
revision=1
distfiles="http://www.h5l.org/dist/src/$pkgname-$version.tar.gz"
build_style=gnu_configure
configure_args="--enable-shared=yes --without-x --datadir=/var/lib/heimdal
--localstatedir=/var/lib/heimdal --enable-pthread-support --with-openssl=/usr
--enable-kcm --with-hdbdir=/var/lib/heimdal --with-readline=/usr"
short_desc="Heimdal Kerberos implementation"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=64ebe73eea0bb4a9a80e038a953807416751451ade3e53f9103d3263b81ba925
long_desc="
 Heimdal is an implementation of Kerberos 5 (and some more stuff) largely
 written in Sweden (which was important when we started writing it, less so
 now). It is freely available under a three clause BSD style license."

subpackages="heimdal-devel heimdal-libs"
conf_files="/etc/krb5.conf"

Add_dependency run glibc
Add_dependency run db
Add_dependency run openssl
Add_dependency run readline
Add_dependency run e2fsprogs-libs
Add_dependency run ncurses-libs
Add_dependency run heimdal-libs

Add_dependency build automake
Add_dependency build flex
Add_dependency build db-devel
Add_dependency build openssl-devel
Add_dependency build e2fsprogs-devel	">=1.4.10_1"
Add_dependency build readline-devel
Add_dependency build ncurses-devel

pre_configure()
{
	sed -i -e 's|var/heimdal|var/lib/heimdal|g' configure.in \
		doc/setup.texi doc/heimdal.info kadmin/kadmind.8 kdc/kdc.8 \
		lib/hdb/hdb.h lib/krb5/krb5.conf.5 lib/krb5/krb5.conf.cat5

	libtoolize --force || return 1
	aclocal -I cf || return 1
	autoconf || return 1
	automake || return 1
}

post_install()
{
	# Rename daemons and their manpages
	for i in telnetd ftpd rshd; do
		mv ${DESTDIR}/usr/share/man/man8/{,k}${i}.8 || return 1
		mv ${DESTDIR}/usr/libexec/{,k}${i} || return 1
	done

	# Rename clients and their manpages
	for i in rcp rsh telnet ftp su login; do
		if [ -f ${DESTDIR}/usr/share/man/man1/${i}.1 ]; then
			mv ${DESTDIR}/usr/share/man/man1/{,k}${i}.1 || return 1
		fi
		mv ${DESTDIR}/usr/bin/{,k}${i} || return 1
	done
	rm -rf ${DESTDIR}/usr/share/man/cat{1,3,5,8}

	# Remove conflicts
	rm ${DESTDIR}/usr/share/man/man5/ftpusers.5*
	rm ${DESTDIR}/usr/share/man/man3/{DES,DH,EVP,OpenSSL,RAND,RSA}*
	rm ${DESTDIR}/usr/share/man/man3/os.3*

	install -d ${DESTDIR}/var/lib/heimdal
	touch ${DESTDIR}/var/lib/heimdal/.empty_on_purpose

	# Install rc services and config files.
	install -D -m644 ${wrksrc}/krb5.conf ${DESTDIR}/etc/krb5.conf
	install -D -m755 ${FILESDIR}/kdc.rc ${DESTDIR}/etc/init.d/kdc
	install -D -m755 ${FILESDIR}/kadmind.rc ${DESTDIR}/etc/init.d/kadmind
	install -D -m755 ${FILESDIR}/kpasswdd.rc ${DESTDIR}/etc/init.d/kpasswdd
}
