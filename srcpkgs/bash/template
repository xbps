# Template build file for 'bash'.
pkgname=bash
bash_patchlevel=005
bash_distver=4.1
version=${bash_distver}.${bash_patchlevel}
wrksrc=${pkgname}-${bash_distver}
distfiles="http://ftp.gnu.org/gnu/bash/bash-${bash_distver}.tar.gz"
build_style=gnu_configure
configure_args="--without-bash-malloc --with-curses --bindir=/bin
 --enable-array-variables --enable-brace-expansion --enable-casemod-attributes
 --enable-casemod-expansions --enable-command-timing --enable-cond-command
 --enable-cond-regexp --enable-coprocesses --enable-debugger
 --enable-directory-stack --enable-dparen-arithmetic --enable-extended-glob
 --enable-help-builtin --enable-history --enable-job-control --enable-multibyte
 --enable-process-substitution --enable-progcomp --enable-prompt-string-decoding
 --enable-select --enable-net-redirections --enable-restricted
 ac_cv_func_working_mktime=yes"
short_desc="The GNU Bourne Again Shell"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=3f627124a83c6d34db503a923e20710d370573a29dd5d11d6f116d1aee7be1da
long_desc="
 Bash is an sh-compatible shell that incorporates useful features from
 the Korn shell (ksh) and C shell (csh). It is intended to conform to
 the IEEE POSIX P1003.2/ISO 9945.2 Shell and Tools standard.

 It offers functional improvements over sh for both programming and
 interactive use; these include command line editing, unlimited size
 command history, job control, shell functions and aliases, indexed
 arrays of unlimited size, and integer arithmetic in any base from two
 to sixty-four. In addition, most sh scripts can be run by Bash without
 modification."

base_chroot=yes
register_shell="/bin/bash"

Add_dependency run	glibc
Add_dependency build	ncurses
Add_dependency run	ncurses-libs
Add_dependency run	coreutils

pre_configure()
{
	local url="http://ftp.gnu.org/gnu/bash/bash-4.1-patches"

	if [ "${bash_patchlevel}" -gt 000 ]; then
		cd ${XBPS_SRCDISTDIR} || return 1
		for p in $(seq -w 001 ${bash_patchlevel}); do
			[ -f ${XBPS_SRCDISTDIR}/bash41-${p} ] && continue
			$XBPS_FETCH_CMD ${url}/bash41-$p
		done
	fi
	cd ${wrksrc}
	for p in $(seq -w 001 ${bash_patchlevel}); do
		patch -s -Np0 -i ${XBPS_SRCDISTDIR}/bash41-${p} && \
			msg_normal "Applying patch bash40-$p."
	done
}
