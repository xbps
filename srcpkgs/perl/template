# Template build file for 'perl'.
pkgname=perl
version=5.10.1
distfiles="http://www.cpan.org/src/$pkgname-$version.tar.gz"
build_style=configure
configure_script="./Configure"
short_desc="Practical Extraction and Report Language"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=cb7f26ea4b2b28d6644354d87a269d01cac1b635287dae64e88eeafa24b44f35
long_desc="
 Perl is a general-purpose programming language originally developed
 for text manipulation and now used for a wide range of tasks including
 system administration, web development, network programming, GUI
 development, and more.  The language is intended to be practical (easy
 to use, efficient, complete) rather than beautiful (tiny, elegant,
 minimal).  Its major features are that it's easy to use, supports both
 procedural and object-oriented (OO) programming, has powerful built-in
 support for text processing, and has one of the world's most impressive
 collections of third-party modules."

Add_dependency run glibc
Add_dependency run gdbm
Add_dependency run db
Add_dependency build gdbm-devel
Add_dependency build db-devel

pre_configure()
{
	local p5_base p5_apiver args

	cd $wrksrc || return 1
	# Taken from NetBSD.
	p5_base="/usr/lib/perl5"
	p5_apiver="$(awk '/\#define[    ]*PERL_API_REVISION/ { R = $3 } \
			  /\#define[    ]*PERL_API_VERSION/ { r = "."$3 } \
			  /\#define[    ]*PERL_API_SUBVERSION/ { s = "."$3 } \
			  END { printf "%s%s%s\n", R, r, s }' patchlevel.h)"

	args="-Dusethreads -des -Dprefix=/usr \
		-Duselargefiles -Uusesfio -Dinstallstyle=lib/perl5 \
		-Uinstallusrbinperl -Duseshrplib \
		-Dman1dir=/usr/share/man/man1 \
		-Dman3dir=/usr/share/man/man3 \
		-Dprivlib=${p5_base}/${p5_apiver} \
		-Dsitelib=${p5_base}/site_perl/${p5_apiver} \
		-Dvendorlib=${p5_base}/vendor_perl/${p5_apiver}"
	export configure_args="${args}"

	sed -i -e "s|'/bin/pwd'|'/usr/local/bin/pwd'|" lib/Cwd.pm
}
