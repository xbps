# Template file for 'nss'
pkgname=nss
version=3.12.6
distfiles="${MOZILLA_SITE}/security/nss/releases/NSS_3_12_6_RTM/src/${pkgname}-${version}.tar.gz"
build_style=custom-install
short_desc="Mozilla Network Security Services"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=8f9759be1ce928e82830923fde62a66e270c4645f10a4c176acfccb6021a9795
long_desc="
 Network Security Services (NSS) is a set of libraries designed to support
 cross-platform development of security-enabled server applications.
 Applications built with NSS can support SSL v2 and v3, TLS, PKCS \#5,
 PKCS \#7, PKCS \#11, PKCS \#12, S/MIME, X.509 v3 certificates, and other
 security standards."

subpackages="$pkgname-devel"
Add_dependency run glibc
Add_dependency run zlib
Add_dependency run nspr
Add_dependency run sqlite
Add_dependency build zlib-devel
Add_dependency build nspr-devel
Add_dependency build sqlite-devel

do_build()
{
	cd ${wrksrc} || return 1
	if [ "$xbps_machine" = "x86_64" ]; then
		_use64="USE_64=1"
	fi
	for dir in coreconf dbm nss; do
		make -C mozilla/security/${dir} \
			LIBRUNPATH= BUILD_OPT=1 \
			NSS_USE_SYSTEM_SQLITE=1 \
			NSPR_INCLUDE_DIR=/usr/include/nspr \
			NSPR_LIB_DIR=/usr/lib ${_use64} || return 1
	done
}

do_install()
{
	cd ${wrksrc} || return 1

	install -d ${DESTDIR}/usr/include/nss \
		${DESTDIR}/usr/lib/pkgconfig
	install -m644 mozilla/dist/public/nss/*.h \
		${DESTDIR}/usr/include/nss || return 1
	install -m755 mozilla/dist/*.OBJ/lib/*.so \
		${DESTDIR}/usr/lib || return 1
	for f in libcrmf.a libnssb.a libnssckfw.a; do
		install -m644 mozilla/dist/*.OBJ/lib/${f} \
			${DESTDIR}/usr/lib || return 1
	done

	( \
	  echo "prefix=/usr"; \
	  echo "exec_prefix=/usr/bin"; \
	  echo "libdir=/usr/lib"; \
	  echo "includedir=/usr/include/nss"; \
	  echo; \
	  echo "Name: NSS"; \
	  echo "Description: Mozilla Network Security Services"; \
	  echo "Version: ${version}"; \
	  echo "Requires: nspr >= ${version}"; \
	  echo "Cflags: -I${includedir}"; \
	  echo "Libs: -lnss3 -lsmime3 -lssl3 -lnssutil3 -lsoftokn3 -lpthread -ldl"; \
	) >${DESTDIR}/usr/lib/pkgconfig/nss.pc
	chmod 644 ${DESTDIR}/usr/lib/pkgconfig/nss.pc || return 1
	cd ${DESTDIR}/usr/lib/pkgconfig && ln -s nss.pc mozilla-nss.pc \
		|| return 1

	cd ${wrksrc} || return 1
	mkdir -p ${DESTDIR}/usr/bin || return 1

	NSS_VMAJOR=$(grep "#define.*NSS_VMAJOR" mozilla/security/nss/lib/nss/nss.h | awk '{print $3}')
	NSS_VMINOR=$(grep "#define.*NSS_VMINOR" mozilla/security/nss/lib/nss/nss.h | awk '{print $3}')
	NSS_VPATCH=$(grep "#define.*NSS_VPATCH" mozilla/security/nss/lib/nss/nss.h | awk '{print $3}')
	sed "${FILESDIR}/nss-config.in" \
		-e "s,@MOD_MAJOR_VERSION@,${NSS_VMAJOR},g" \
		-e "s,@MOD_MINOR_VERSION@,${NSS_VMINOR},g" \
		-e "s,@MOD_PATCH_VERSION@,${NSS_VPATCH},g" \
		> "${DESTDIR}/usr/bin/nss-config" || return 1
	chmod 755 "${DESTDIR}/usr/bin/nss-config" || return 1
}
