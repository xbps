#!/bin/sh
#-
# Copyright (c) 2010 Juan Romero Pardines.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#-

HANDLER="$1"

. @@XBPS_INSTALL_ETCDIR@@/xbps-src.conf

if [ -n "${MASTERDIR}" ]; then
	export XBPS_MASTERDIR="${MASTERDIR}"
fi

# Umount stuff if SIGINT or SIGQUIT was caught
trap umount_chroot_fs INT QUIT

REQFS="sys proc dev xbps"

mount_chroot_fs()
{
	local cnt f blah dowrite

	for f in ${REQFS}; do
		if [ ! -f ${XBPS_MASTERDIR}/.${f}_mount_bind_done ]; then
			unset dowrite
			echo -n "=> Mounting /${f} in chroot... "
			if [ ! -d ${XBPS_MASTERDIR}/${f} ]; then
				mkdir -p ${XBPS_MASTERDIR}/${f}
			fi
			case ${f} in
				xbps)
					blah=${XBPS_DISTRIBUTIONDIR}
					dowrite="-w"
					;;
				*) blah=/${f};;
			esac
			[ ! -d ${blah} ] && echo "failed." && continue
			@@XBPS_INSTALL_LIBEXECDIR@@/xbps-src-chroot-capmount \
				${dowrite} ${blah} ${XBPS_MASTERDIR}/${f} \
				2>/dev/null
			if [ $? -eq 0 ]; then
				echo 1 > ${XBPS_MASTERDIR}/.${f}_mount_bind_done
				echo "done."
			else
				echo "failed."
			fi
		else
			cnt=$(cat ${XBPS_MASTERDIR}/.${f}_mount_bind_done)
			cnt=$((${cnt} + 1))
			echo ${cnt} > ${XBPS_MASTERDIR}/.${f}_mount_bind_done
		fi
	done
}

umount_chroot_fs()
{
	local fs dir cnt

	for fs in ${REQFS}; do
		[ ! -f ${XBPS_MASTERDIR}/.${fs}_mount_bind_done ] && continue
		cnt=$(cat ${XBPS_MASTERDIR}/.${fs}_mount_bind_done)
		if [ ${cnt} -gt 1 ]; then
			cnt=$((${cnt} - 1))
			echo ${cnt} > ${XBPS_MASTERDIR}/.${fs}_mount_bind_done
		else
			echo -n "=> Unmounting ${fs} from chroot... "
			@@XBPS_INSTALL_LIBEXECDIR@@/xbps-src-chroot-capumount \
				${XBPS_MASTERDIR}/${fs} 2>/dev/null
			if [ $? -eq 0 ]; then
				rm -f ${XBPS_MASTERDIR}/.${fs}_mount_bind_done
				echo "done."
			else
				echo "failed."
			fi
		fi
		unset fs
	done

	# Remove created dirs
	[ -f ${XBPS_MASTERDIR}/.xbps_mount_bind_done ] && continue
	[ -d ${XBPS_MASTERDIR}/xbps ] && rmdir ${XBPS_MASTERDIR}/xbps
}

if [ $# -ne 1 ]; then
	echo "$0: mount || umount"
	exit 1
fi

case "${HANDLER}" in
	mount) mount_chroot_fs;;
	umount) umount_chroot_fs;;
	*) echo "$0: invalid target." && exit 1;;
esac

exit 0
