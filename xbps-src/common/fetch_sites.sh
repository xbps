# 
# HTTP/FTP vars that can be used by templates.
#

SOURCEFORGE_SITE="http://downloads.sourceforge.net/sourceforge"
NONGNU_SITE="http://download.savannah.nongnu.org/releases"
UBUNTU_SITE="http://archive.ubuntu.com/ubuntu/pool"
XORG_SITE="http://xorg.freedesktop.org/releases/individual"
DEBIAN_SITE="http://ftp.debian.org/debian/pool"
GNOME_SITE="http://ftp.gnome.org/pub/GNOME/sources"
KERNEL_SITE="http://www.kernel.org/pub/linux"
CPAN_SITE="http://cpan.perl.org/modules/by-module"
MOZILLA_SITE="ftp://ftp.mozilla.org/pub/mozilla.org"
